open Ppxlib
open Ast_builder.Default

let dir = ref ""
let tzfunc = ref true
let output = ref None

let defines : (string, unit) Hashtbl.t = Hashtbl.create 512

type kind = [ `entry | `param | `store ]
type element = {
  source_file : string;
  type_name : string;
  main : bool;
  kind : kind;
}

let entrypoints : (string, element) Hashtbl.t = Hashtbl.create 512

let spf ~loc l =
  eapply ~loc (evar ~loc "Format.sprintf") l

let spfs ~loc s =
  spf ~loc [estring ~loc s]

let var =
  let i = ref 0 in fun () -> incr i; "v" ^ string_of_int !i

let remove_prefix s n = String.sub s n (String.length s - n)

let same_prefix l =
  let common_prefix s1 s2 =
    let n1 = String.length s1 in
    let n2 = String.length s2 in
    let rec aux i =
      if i < n1 && i < n2 && s1.[i] = s2.[i] then aux (i+1)
      else i, String.sub s1 0 i in
    aux 0 in
  let rec aux n pr = function
    | [] -> n, pr
    | h :: t ->
      let n, pr = common_prefix h pr in
      aux n pr t in
  match l with
  | [] | [ _ ] -> 0
  | h :: t -> fst (aux (String.length h) h t)

let rec constr ~loc ?(allow_unknown=true) txt l = match txt, l with
  | "unit", _ -> Some (pexp_fun ~loc Nolabel None (ppat_any ~loc) @@ estring ~loc "()")
  | "int", _ -> Some (spfs ~loc "%d")
  | "nat", _ -> Some (spfs ~loc "%dn")
  | "tez", _ -> Some (spfs ~loc "%dmutez")
  | "address", _ -> Some (spfs ~loc "(%s : address)")
  | "key", _ -> Some (spfs ~loc "(%s : key)")
  | "key_hash", _ -> Some (spfs ~loc "(%s : key_hash)")
  | "signature", _ -> Some (spfs ~loc "(%s : signature)")
  | "chain_id", _ -> Some (spfs ~loc "(%s : chain_id)")
  | "string", _ -> Some (spfs ~loc "%s")
  | "bytes", _ -> Some (spfs ~loc "%s")
  | "bool", _ -> Some (spfs ~loc "%B")
  | "option", [ c ] ->
    begin match core ~loc:c.ptyp_loc ~allow_unknown c.ptyp_desc with
      | None -> None
      | Some e ->
        let v = var () in
        Some (
          pexp_fun ~loc Nolabel None (pvar ~loc v) @@
          pexp_match ~loc (evar ~loc v) [
            case ~guard:None
              ~lhs:(ppat_construct ~loc {txt=Lident "None";loc} None)
              ~rhs:(estring ~loc "None");
            case ~guard:None
              ~lhs:(ppat_construct ~loc {txt=Lident "Some"; loc} (Some (pvar ~loc v)))
              ~rhs:(spf ~loc [estring ~loc "Some %s"; eapply ~loc e [ evar ~loc v ]])])
    end
  | "list", [ c ] ->
    begin match core ~loc:c.ptyp_loc ~allow_unknown c.ptyp_desc with
      | None -> None
      | Some e ->
        let l = var () in
        Some (pexp_fun ~loc Nolabel None (pvar ~loc l) @@
              spf ~loc [
                estring ~loc "[%s]";
                eapply ~loc (evar ~loc "String.concat") [
                  estring ~loc ";";
                  eapply ~loc (evar ~loc "List.map") [ e; evar ~loc l ]
                ]
              ])
    end
  | "map", [ k; v ] | "big_map", [k; v] ->
    let ck = core ~loc:k.ptyp_loc ~allow_unknown k.ptyp_desc in
    let cv = core ~loc:v.ptyp_loc ~allow_unknown v.ptyp_desc in
    begin match ck, cv with
      | None, _ | _, None -> None
      | Some ek, Some ev ->
        let l = var () in
        Some (pexp_fun ~loc Nolabel None (pvar ~loc l) @@
              spf ~loc [
                estring ~loc @@ (String.capitalize_ascii txt) ^ ".literal [%s]";
                eapply ~loc (evar ~loc "String.concat") [
                  estring ~loc ";";
                  eapply ~loc (evar ~loc "List.map") [
                    pexp_fun ~loc Nolabel None (ppat_tuple ~loc [pvar ~loc "k"; pvar ~loc "v"]) @@
                    eapply ~loc (evar ~loc "String.concat") [
                      estring ~loc ",";
                      elist ~loc [ eapply ~loc ek [ evar ~loc "k" ];  eapply ~loc ev [ evar ~loc "v" ] ];
                    ];
                    evar ~loc l
                  ]
                ]
              ])
    end
  | "set", [ c ] ->
    begin match core ~loc:c.ptyp_loc ~allow_unknown c.ptyp_desc with
      | None -> None
      | Some e ->
        let l = var () in
        Some (pexp_fun ~loc Nolabel None (pvar ~loc l) @@
              spf ~loc [
                estring ~loc "Set.literal [%s]";
                eapply ~loc (evar ~loc "String.concat") [
                  estring ~loc ";";
                  eapply ~loc (evar ~loc "List.map") [ e; evar ~loc l ] ] ])
    end
  | s, [] -> if allow_unknown then Some (evar ~loc (s ^ "_repr")) else None
  | _ -> None

and core ~loc ?allow_unknown = function
  | Ptyp_constr ({txt; loc}, l) ->
    constr ~loc ?allow_unknown (Longident.name txt) l
  | Ptyp_tuple l ->
    let vs, cs = List.split @@ List.filter_map (fun c ->
        match core ~loc ?allow_unknown c.ptyp_desc with
        | None -> None
        | Some e -> Some (var (), e)) l in
    if List.length cs <> List.length l then None
    else
      Some (pexp_fun ~loc Nolabel None (ppat_tuple ~loc @@ List.map (pvar ~loc) vs)
              (eapply ~loc (evar ~loc "String.concat") [
                  estring ~loc ",";
                  elist ~loc @@ List.map2 (fun c v -> eapply ~loc c [evar ~loc v]) cs vs]))
  | _ -> failwith "core type not handled"

let field ?(rm_prefix=0) pld =
  let loc = pld.pld_loc in
  let alias =
    List.fold_left (fun acc a -> match a.attr_name.txt, a.attr_payload with
        | "key", PStr [ {pstr_desc = Pstr_eval (
            {pexp_desc = Pexp_constant (Pconst_string (s, _, _)); _}, _); _} ] ->
          Some s
        | _ -> acc) None pld.pld_attributes in
  let alias = match alias, rm_prefix with
    | Some a, _ -> Some a
    | _, 0 -> None
    | _, n -> Some (remove_prefix pld.pld_name.txt n) in
  match core ~loc pld.pld_type.ptyp_desc with
  | None -> failwith @@ Format.sprintf "cannot derive expression for %S" pld.pld_name.txt
  | Some c -> pld.pld_name.txt, alias, c

let record ~loc l =
  let v = var () in
  let rm_prefix = same_prefix @@ List.map (fun pld -> pld.pld_name.txt) l in
  let l = List.map (field ~rm_prefix) l in
  pexp_fun ~loc Nolabel None (pvar ~loc v) @@
  spf ~loc (
    (estring ~loc @@ Format.sprintf "{%s}" @@ String.concat "; " @@
     List.map (function (_, Some alias, _) -> alias ^ "=%s" | (n, _, _) -> n ^ "=%s") l) ::
    List.map (fun (n, _, c) -> eapply ~loc c
                 [ pexp_field ~loc (evar ~loc v) {txt = Lident n; loc} ]) l)

let constructor pcd =
  let loc = pcd.pcd_loc in
  let allow_unknown = match Hashtbl.find_opt entrypoints pcd.pcd_name.txt with
    | Some e ->
      Hashtbl.replace entrypoints pcd.pcd_name.txt {e with main = false};
      true
    | None -> false in
  let name = pcd.pcd_name.txt in
  match pcd.pcd_args with
  | Pcstr_tuple [] -> Some (name, None)
  | Pcstr_tuple [c] ->
    begin match core ~loc ~allow_unknown c.ptyp_desc with
      | None -> None
      | Some e -> Some (name, Some e)
    end
  | Pcstr_tuple l ->
    begin match core ~loc ~allow_unknown (Ptyp_tuple l) with
      | None -> None
      | Some e -> Some (name, Some e)
    end
  | _ -> failwith "Record constructor not handled"

let variant ~loc l =
  let all, l = List.fold_left (fun (acc_all, acc_l) pcd -> match constructor pcd with
      | None -> false, acc_l
      | Some x -> acc_all, x :: acc_l)  (true, []) l in
  pexp_function ~loc @@
  (List.map (fun (cs, e) ->
       let v = var () in
       let lhs = ppat_construct ~loc {txt=Longident.parse cs; loc}
           (Option.map (fun _ -> pvar ~loc v) e) in
       let rhs = match e with
         | None -> estring ~loc cs
         | Some e -> spf ~loc [
             estring ~loc (cs ^ " %s"); eapply ~loc e [ evar ~loc v ] ] in
       case ~guard:None ~lhs ~rhs

     ) (List.rev l)) @
  (if not all then [
      case ~guard:None ~lhs:(ppat_any ~loc)
        ~rhs:(eapply ~loc (evar ~loc "failwith") [ estring ~loc "no case matched" ]) ]
   else [])

let do_derive source_file p =
  match List.find_opt (fun a -> a.attr_name.txt = "entry" || a.attr_name.txt = "store" || a.attr_name.txt = "param") p.ptype_attributes with
  | None -> false
  | Some a -> match a.attr_payload, a.attr_name.txt with
    | _, "store" ->
      let e = {source_file; type_name=p.ptype_name.txt; main=false; kind=`store} in
      Hashtbl.add entrypoints "Store" e;
      true
    | PStr [{pstr_desc=Pstr_eval ({pexp_desc = Pexp_construct ({txt; _}, _); _}, _); _} ], _ ->
      let kind = match a.attr_name.txt with "entry" -> `entry | "store" -> `store | "param" -> `param | _ -> assert false in
      let e = {source_file; type_name=p.ptype_name.txt; main=true; kind} in
      Hashtbl.add entrypoints (Longident.name txt) e;
      true
    | _ -> false

let repr_expr p =
  let loc = p.ptype_loc in
  let expr = match p.ptype_kind, p.ptype_manifest with
    | Ptype_abstract, Some m ->
      Option.get @@ core ~loc m.ptyp_desc
    | Ptype_variant l, _ ->
      variant ~loc l
    | Ptype_record l, _ ->
      record ~loc l
    | _ -> Location.raise_errorf "Type not handled" in
  let pat = pvar ~loc (p.ptype_name.txt ^ "_repr") in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
    ]

let parg_optional ~loc ?expr s =
  pexp_fun ~loc (Optional s) expr (pvar ~loc s)
let parg_labelled ~loc ?expr s =
  pexp_fun ~loc (Labelled s) expr (pvar ~loc s)
let earg_optional ~loc s = Optional s, evar ~loc s
let earg_labelled ~loc s = Labelled s, evar ~loc s

let compile_expr ?(kind=`parameter) ?entrypoint ?source_file type_name =
  let loc = !Ast_helper.default_loc in
  let entrypoint_expr = Option.map (estring ~loc) entrypoint in
  let source_file_expr = Option.map (fun s -> estring ~loc (Filename.concat !dir (s ^ ".mligo"))) source_file in
  let expr =
    parg_optional ~loc "format" @@
    parg_optional ~loc ?expr:entrypoint_expr "entrypoint" @@
    parg_optional ~loc ?expr:source_file_expr "source_file" @@
    pexp_fun ~loc Nolabel None (pvar ~loc "p") @@
    pexp_apply ~loc (evar ~loc "Mligo_unix.compile_repr") [
      earg_optional ~loc "format";
      earg_labelled ~loc "entrypoint";
      earg_labelled ~loc "source_file";
      Labelled "kind", pexp_variant ~loc (match kind with `parameter -> "parameter" | `storage -> "storage") None;
      Labelled "expr", eapply ~loc (evar ~loc (type_name ^ "_repr")) [ evar ~loc "p" ];
      Nolabel, eunit ~loc
    ] in
  let pat = pvar ~loc ("compile_" ^ type_name) in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
  ]

let make_op_expr ?(kind=`parameter) ?entrypoint ?source_file type_name =
  let loc = !Ast_helper.default_loc in
  let entrypoint_expr = Option.map (estring ~loc) entrypoint in
  let source_file_expr = Option.map (fun s -> estring ~loc (Filename.concat !dir (s ^ ".mligo"))) source_file in
  let amount, op, parg_contract, earg_contract = match kind with
    | `parameter -> "amount", "tr", parg_labelled ~loc "contract", [ earg_labelled ~loc "contract" ]
    | `storage -> "balance", "or", (fun e -> e), [] in
  let expr =
    parg_optional ~loc ?expr:entrypoint_expr "entrypoint" @@
    parg_optional ~loc ?expr:source_file_expr "source_file" @@
    parg_optional ~loc "fee" @@
    parg_optional ~loc "gas_limit" @@
    parg_optional ~loc "storage_limit" @@
    parg_optional ~loc "counter" @@
    parg_optional ~loc amount @@
    parg_labelled ~loc "source" @@
    parg_contract @@
    pexp_fun ~loc Nolabel None (pvar ~loc "p") @@
    pexp_apply ~loc (evar ~loc ("Mligo_tzfunc.make_" ^ op)) ([
        earg_labelled ~loc "entrypoint";
        earg_labelled ~loc "source_file";
        earg_optional ~loc "fee";
        earg_optional ~loc "gas_limit";
        earg_optional ~loc "storage_limit";
        earg_optional ~loc "counter";
        earg_optional ~loc amount;
        earg_labelled ~loc "source" ] @ earg_contract @ [
          Nolabel, evar ~loc (type_name ^ "_repr");
          Nolabel, evar ~loc "p"
        ]) in
  let pat = pvar ~loc ("op_" ^ type_name) in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
  ]

let forge_op_expr ?(kind=`parameter) ?entrypoint ?source_file type_name =
  let loc = !Ast_helper.default_loc in
  let entrypoint_expr = Option.map (estring ~loc) entrypoint in
  let source_file_expr = Option.map (fun s -> estring ~loc (Filename.concat !dir (s ^ ".mligo"))) source_file in
  let amount, op, parg_contract, earg_contract = match kind with
    | `parameter -> "amount", "tr", parg_labelled ~loc "contract", [ earg_labelled ~loc "contract" ]
    | `storage -> "balance", "or", (fun e -> e), [] in
  let expr =
    parg_optional ~loc ?expr:entrypoint_expr "entrypoint" @@
    parg_optional ~loc ?expr:source_file_expr "source_file" @@
    parg_optional ~loc "fee" @@
    parg_optional ~loc "gas_limit" @@
    parg_optional ~loc "storage_limit" @@
    parg_optional ~loc "counter" @@
    parg_optional ~loc amount @@
    parg_optional ~loc "remove_failed" @@
    parg_optional ~loc "forge_method" @@
    parg_optional ~loc "base" @@
    parg_optional ~loc "get_pk" @@
    parg_labelled ~loc "source" @@
    parg_contract @@
    pexp_fun ~loc Nolabel None (pvar ~loc "p") @@
    pexp_apply ~loc (evar ~loc ("Mligo_tzfunc.forge_" ^ op)) ([
      earg_labelled ~loc "entrypoint";
      earg_labelled ~loc "source_file";
      earg_optional ~loc "fee";
      earg_optional ~loc "gas_limit";
      earg_optional ~loc "storage_limit";
      earg_optional ~loc "counter";
      earg_optional ~loc amount;
      earg_optional ~loc "remove_failed";
      earg_optional ~loc "forge_method";
      earg_optional ~loc "base";
      earg_optional ~loc "get_pk";
      earg_labelled ~loc "source" ] @ earg_contract @ [
      Nolabel, evar ~loc (type_name ^ "_repr");
      Nolabel, evar ~loc "p"
    ]) in
  let pat = pvar ~loc ("forge_" ^ type_name) in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
  ]

let call_expr ?entrypoint ?source_file type_name =
  let loc = !Ast_helper.default_loc in
  let entrypoint_expr = Option.map (estring ~loc) entrypoint in
  let source_file_expr = Option.map (fun s -> estring ~loc (Filename.concat !dir (s ^ ".mligo"))) source_file in
  let expr =
    parg_optional ~loc ?expr:entrypoint_expr "entrypoint" @@
    parg_optional ~loc ?expr:source_file_expr "source_file" @@
    parg_optional ~loc "fee" @@
    parg_optional ~loc "gas_limit" @@
    parg_optional ~loc "storage_limit" @@
    parg_optional ~loc "counter" @@
    parg_optional ~loc "amount" @@
    parg_optional ~loc "remove_failed" @@
    parg_optional ~loc "forge_method" @@
    parg_optional ~loc "base" @@
    parg_optional ~loc "get_pk" @@
    parg_labelled ~loc "source" @@
    parg_labelled ~loc "contract" @@
    parg_labelled ~loc "sign" @@
    pexp_fun ~loc Nolabel None (pvar ~loc "p") @@
    pexp_apply ~loc (evar ~loc "Mligo_tzfunc.call") [
      earg_labelled ~loc "entrypoint";
      earg_labelled ~loc "source_file";
      earg_optional ~loc "fee";
      earg_optional ~loc "gas_limit";
      earg_optional ~loc "storage_limit";
      earg_optional ~loc "counter";
      earg_optional ~loc "amount";
      earg_optional ~loc "remove_failed";
      earg_optional ~loc "forge_method";
      earg_optional ~loc "base";
      earg_optional ~loc "get_pk";
      earg_labelled ~loc "source";
      earg_labelled ~loc "contract";
      earg_labelled ~loc "sign";
      Nolabel, evar ~loc (type_name ^ "_repr");
      Nolabel, evar ~loc "p"
    ] in
  let pat = pvar ~loc ("call_" ^ type_name) in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
  ]

let deploy_expr ?entrypoint ?source_file type_name =
  let loc = !Ast_helper.default_loc in
  let entrypoint_expr = Option.map (estring ~loc) entrypoint in
  let source_file_expr = Option.map (fun s -> estring ~loc (Filename.concat !dir (s ^ ".mligo"))) source_file in
  let expr =
    parg_optional ~loc ?expr:entrypoint_expr "entrypoint" @@
    parg_optional ~loc ?expr:source_file_expr "source_file" @@
    parg_optional ~loc "fee" @@
    parg_optional ~loc "gas_limit" @@
    parg_optional ~loc "storage_limit" @@
    parg_optional ~loc "counter" @@
    parg_optional ~loc "balance" @@
    parg_optional ~loc "remove_failed" @@
    parg_optional ~loc "forge_method" @@
    parg_optional ~loc "base" @@
    parg_optional ~loc "get_pk" @@
    parg_labelled ~loc "source" @@
    parg_labelled ~loc "sign" @@
    pexp_fun ~loc Nolabel None (pvar ~loc "storage") @@
    pexp_apply ~loc (evar ~loc "Mligo_tzfunc.deploy") [
      earg_labelled ~loc "entrypoint";
      earg_labelled ~loc "source_file";
      earg_optional ~loc "fee";
      earg_optional ~loc "gas_limit";
      earg_optional ~loc "storage_limit";
      earg_optional ~loc "counter";
      earg_optional ~loc "balance";
      earg_optional ~loc "remove_failed";
      earg_optional ~loc "forge_method";
      earg_optional ~loc "base";
      earg_optional ~loc "get_pk";
      earg_labelled ~loc "source";
      earg_labelled ~loc "sign";
      Nolabel, evar ~loc (type_name ^ "_repr");
      Nolabel, evar ~loc "storage"
    ] in
  let pat = pvar ~loc "deploy" in
  pstr_value ~loc Nonrecursive [
    value_binding ~loc ~pat ~expr
  ]

module SSet = Set.Make(String)

let s = ref SSet.empty

let type_declaration filename p =
  if not (do_derive filename p) || SSet.mem p.ptype_name.txt !s then []
  else (
    s := SSet.add p.ptype_name.txt !s;
    [ repr_expr p ])

let rec derive filename =
  object(_self)
    inherit [structure] Ast_traverse.fold as super
    method! type_declaration p acc = acc @ type_declaration filename p
    method! include_declaration i acc =
      match i.pincl_mod.pmod_desc with
      | Pmod_ident {txt = Lident id; _} -> process (String.uncapitalize_ascii id) @ acc
      | _ -> super#include_declaration i acc
    method! structure s acc =
      let rec choose = function
        | (Some env_var, l) :: t ->
          begin match Sys.getenv_opt env_var, Hashtbl.find_opt defines env_var with
            | Some "true", _ | Some "1", _ | _, Some () -> l
            | _ -> choose t
          end
        | [ None, l ] -> l
        | _ -> Location.raise_errorf "missing [%%%%if] or [%%%%else]" in
      let s, _ = List.fold_left (fun (acc, acc_ext) x ->
          match x.pstr_desc with
          | Pstr_extension (({txt; _}, PStr s), _) ->
            if String.length txt > 3 && String.sub txt 0 2 = "if" && acc_ext = [] then
              let env_var = String.sub txt 3 (String.length txt - 3) in
              (acc, [Some env_var, s])
            else if String.length txt > 5 && String.sub txt 0 4 = "elif" && acc_ext <> [] then
              let env_var = String.sub txt 5 (String.length txt - 5) in
              (acc, (Some env_var, s) :: acc_ext)
            else if txt = "else" then
              let acc_ext = List.rev @@ (None, s) :: acc_ext in
              let l = List.fold_left (fun acc si ->
                  super#structure_item si acc) [] (choose acc_ext) in
              (acc @ l, [])
            else Location.raise_errorf "extension not handled"
          | Pstr_attribute a when a.attr_name.txt = "define" ->
            begin match a.attr_payload with
              | PStr [{pstr_desc=Pstr_eval ({pexp_desc=Pexp_construct ({txt=Lident x;_}, None); _}, _); _}]
              | PStr [{pstr_desc=Pstr_eval ({pexp_desc=Pexp_ident {txt=Lident x;_}; _}, _); _}] ->
                Hashtbl.add defines x ()
              | _ -> ()
            end;
            acc, []
          | _ ->
            if acc_ext = [] then super#structure_item x acc, []
            else Location.raise_errorf "something wrong in [%%%%if] extenstion"
        ) (acc, []) s in
      s
  end

and process filename =
  let path = Filename.concat !dir filename in
  let ic = open_in (path ^ ".ml") in
  let s = really_input_string ic (in_channel_length ic) in
  close_in ic;
  let lexbuf = Lexing.from_string s in
  let str = Parse.implementation lexbuf in
  (derive filename)#structure str []

let more_str () =
  fst @@ Hashtbl.fold (fun name {source_file; type_name; kind; _} (acc, l) ->
      if List.mem name l then acc, l
      else
        match kind with
        | `entry ->
          let entrypoint = String.uncapitalize_ascii name in
          let it_compile = compile_expr ~source_file ~entrypoint type_name in
          let its =
            if !tzfunc then
              let it_make_tr = make_op_expr ~source_file ~entrypoint type_name in
              let it_forge_tr = forge_op_expr ~source_file ~entrypoint type_name in
              let it_call = call_expr ~source_file ~entrypoint type_name in
              [ it_compile; it_make_tr; it_forge_tr; it_call ]
            else
              [ it_compile ] in
          acc @ its, name :: l
        | `store ->
          let entrypoint = String.uncapitalize_ascii name in
          let it_compile = compile_expr ~source_file ~entrypoint ~kind:`storage type_name in
          if !tzfunc then
            let it_make_or = make_op_expr ~source_file ~entrypoint ~kind:`storage type_name in
            let it_forge_or = forge_op_expr ~source_file ~entrypoint ~kind:`storage type_name in
            let it_deploy = deploy_expr ~source_file ~entrypoint type_name in
            acc @ [ it_compile; it_make_or; it_forge_or; it_deploy ], name :: l
          else
            acc, name :: l
        | _ -> acc, name :: l) entrypoints ([], [])


let specs = [
  "-o", Arg.String (fun s -> output := Some s), "output file";
  "-s", Arg.Clear tzfunc, "no tzfunc bindings"
]

let () =
  let filename = ref None in

  Arg.parse specs (fun c -> filename := Some c) "mligo_interface <filename>";
  match !filename with
  | None -> failwith "No file given"
  | Some f ->
    dir := Filename.dirname f;
    let name = Filename.(remove_extension (basename f)) in
    let str_repr = process name in
    let str_compile = more_str () in
    let oc = open_out (Option.value ~default:(Filename.remove_extension f ^ "_interface.ml") !output) in
    let loc = !Ast_helper.default_loc in
    let () = Pprintast.structure Format.str_formatter @@
      pstr_open ~loc (open_infos ~loc ~expr:(
          pmod_ident ~loc {txt=Lident (String.capitalize_ascii name); loc}) ~override:Fresh) ::
      str_repr @ str_compile in
    let s = Format.flush_str_formatter () in
    let fmt = Format.formatter_of_out_channel oc in
    Format.fprintf fmt "%s@." s;
    close_out oc
