open Std
open Bytes

val blake2b : bytes -> bytes
val sha256 : bytes -> bytes
val sha512 : bytes -> bytes
val hash_key : key -> key_hash
val check : key -> signature -> bytes -> bool
