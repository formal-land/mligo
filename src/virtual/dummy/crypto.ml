let blake2b s = Digestif.BLAKE2B.(to_raw_string @@ digest_string s)
let sha256 s = Digestif.SHA256.(to_raw_string @@ digest_string s)
let sha512 s = Digestif.SHA512.(to_raw_string @@ digest_string s)
let hash_key s = match Tzfunc.Crypto.pk_to_pkh s with
  | Ok s -> s
  | Error _ -> failwith "Not a edpk"
let check edpk edsig b =
  match Tzfunc.Crypto.Ed25519.verify ~edpk ~edsig ~bytes:(Tzfunc.Raw.mk b) with
  | Ok b -> b
  | Error _ -> failwith "Not a edpk or not a edsig"
