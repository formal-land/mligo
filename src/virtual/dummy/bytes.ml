type bytes = Stdlib.String.t
let concat a b = a ^ b
let sub i j s = Stdlib.String.sub s i j
let pack x = Marshal.to_string x []
let unpack s = try Some (Marshal.from_string s 0) with _ -> None
let length s = Stdlib.String.length s
