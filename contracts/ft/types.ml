open Mligo
include Errors

(* Fa2 *)

type transfer_destination = {
  tr_dst : address; [@key "to_"]
  tr_token_id : nat;
  tr_amount : nat;
} [@@comb] [@@param Transfer]

type transfer = {
  tr_src : address; [@key "from_"]
  tr_txs : transfer_destination list;
} [@@comb] [@@param Transfer]

type operator_param = {
  op_owner : address;
  op_operator : address;
  op_token_id: nat;
} [@@comb] [@@param Update_operators]

type operator_update =
  | Add_operator of operator_param
  | Remove_operator of operator_param
[@@param Update_operators]

type balance_of_request = {
  ba_owner : address;
  ba_token_id : nat;
} [@@comb]

type balance_of_response = {
  ba_request : balance_of_request;
  ba_balance : nat;
} [@@comb]

type balance_of_param = {
  ba_requests : balance_of_request list;
  ba_callback : (balance_of_response list, unit) contract;
} [@@comb]

type fa2 =
  | Transfer of transfer list
  | Update_operators of operator_update list
  | Balance_of of balance_of_param
[@@entry Assets]

(* Manager *)

type manager =
  | Mint of nat
  | Burn of nat
[@@entry Tokens]

(* Admin *)

type admin =
  | Set_admin of address
  | Confirm_admin
  | Pause of bool
[@@entry Admin]

(* Main *)

type param =
  | Assets of fa2
  | Admin of admin
  | Manager of manager
[@@entry Main]

(* Storage *)

type ledger = (address, nat) big_map [@@param Store]
type operators_storage = (address * address, unit) big_map [@@param Store]
type token_metadata_storage = (nat, nat * (string, bytes) map) big_map [@@param Store]

type storage = {
  admin: address;
  pending_admin: address option;
  paused: bool;
  ledger: ledger;
  operators: operators_storage;
  token_metadata: token_metadata_storage;
  supply: nat;
  metadata: (string, bytes) big_map;
} [@@store]
