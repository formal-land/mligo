open Mligo
include Admin

let validate_mint_param (supply : nat) (owners : (address * nat) list) : unit =
  let res = List.fold
      (fun (acc, (_, amo) : nat * (address * nat)) -> acc + amo) owners 0n in
  if res <> supply then failwith "INVALID_SUPPLY"

let rec add_tokens (owner, id, end_, l : address * nat * nat * ledger) : ledger =
  if id >= end_ then l
  else
    let l = Big_map.add (id, owner) 1n l in
    add_tokens (owner, (id+1n), end_, l)

let mint_tokens (s : storage) (p: mint_param): storage =
  let supply, is_nft = match p.mi_token_def with
    | Nft n -> n, true
    | Multiple n -> n, false in
  let () = validate_mint_param supply p.mi_owners in
  if is_nft then
    if s.next_token_id > p.mi_token_id then (failwith "USED_TOKEN_IDS" : storage)
    else
      let token_metadata = Big_map.add p.mi_token_id (supply, p.mi_token_info) s.token_metadata in
      let (ledger, next_token_id) =
        List.fold (fun ((l, id), (owner, quantity) : (ledger * nat) * (address * nat)) ->
            let end_ = id + quantity in
            let ledger = add_tokens (owner, id, end_, l) in
            ledger, end_
        ) p.mi_owners (s.ledger, p.mi_token_id) in
      { s with token_metadata; next_token_id; ledger }
  else if s.next_token_id > p.mi_token_id then
    let ledger =
      List.fold (fun (l, (owner, amo) : ledger * (address * nat)) ->
          match Big_map.find_opt (p.mi_token_id, owner) l with
          | None -> Big_map.add (p.mi_token_id, owner) amo l
          | Some am -> Big_map.update (p.mi_token_id, owner) (Some (amo + am)) l
        ) p.mi_owners s.ledger in
    { s with ledger }
  else
    let token_metadata = Big_map.add p.mi_token_id (supply, p.mi_token_info) s.token_metadata in
    let ledger =
      List.fold (fun (l, (owner, amo) : ledger * (address * nat)) ->
          Big_map.add (p.mi_token_id, owner) amo l
        ) p.mi_owners s.ledger in
    { s with token_metadata; ledger; next_token_id = p.mi_token_id + 1n }

let rec remove_tokens_nft (owner, id, end_, ledger : address * nat * nat * ledger) : ledger =
  if id >= end_ then ledger
  else
    let ledger = Big_map.remove (id, owner) ledger in
    remove_tokens_nft (owner, (id + 1n), end_, ledger)

let remove_tokens_multiple (owner : address) (id : nat) (amo : nat) (ledger : ledger) : ledger =
  match Big_map.find_opt (id, owner) ledger with
  | None -> (failwith "INVALID_PARAM" : ledger)
  | Some am ->
    match is_nat (am - amo) with
    | None -> (failwith "INVALID_PARAM" : ledger)
    | Some d -> Big_map.update (id, owner) (Some d) ledger

let remove_tokens (s : storage) (id : nat) (owners : (address * nat) list) (is_nft : bool) : storage =
  let ledger =
    if is_nft then
      List.fold (fun ((l, (owner, amo)) : ledger * (address * nat)) ->
          remove_tokens_nft (owner, id, (id + amo), l)) owners s.ledger
    else
      List.fold (fun ((l, (owner, amo)) : ledger * (address * nat)) ->
          remove_tokens_multiple owner id amo l) owners s.ledger in
  let token_metadata = Big_map.remove id s.token_metadata in
  { s with ledger; token_metadata }

let burn_tokens (s : storage) (p : burn_param)  : storage =
  match Big_map.find_opt p.bu_token_id s.token_metadata with
  | None -> (failwith "INVALID_PARAM" : storage)
  | Some (_, m) ->
    if Tezos.get_sender None = s.admin then remove_tokens s p.bu_token_id p.bu_owners p.bu_is_nft
    else if not (Map.mem "burnable" m) then (failwith "NOT_BURNABLE" : storage)
    else match p.bu_owners with
      | [ owner, _ ] ->
        begin match Big_map.find_opt (p.bu_token_id, owner) s.ledger with
          | None -> (failwith "NOT_OWNER" : storage)
          | Some _ ->
            remove_tokens s p.bu_token_id p.bu_owners p.bu_is_nft
        end
      | _ -> (failwith "NOT_AN_ADMIN" : storage)

let manager (param, s : manager * storage) : (operation list) * storage =
  let s = match param with
    | Mint_tokens p ->
      let () = fail_if_not_admin s in
      mint_tokens s p
    | Burn_tokens p -> burn_tokens s p in
  ([] : operation list), s
